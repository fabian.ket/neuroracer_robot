#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from std_msgs.msg import Bool

import os
import sys
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.dirname(
                                os.path.dirname(os.path.abspath(__file__))
                            )
                        )
                    )
                ))

from neuroracer_utils.YamlLoader import load_yaml

SCALE_AXES_X = -1.0 # LEFT/RIGHT values of analog sticks/cross are reversed
SCALE_AXES_Y =  1.0 # UP/DOWN values of analog sticks/cross are normal

class ControllerState(object):
    '''
    Class which saves the current controller state
    '''

    def __init__(self, btn_a_id=0, btn_b_id=1,
                       btn_x_id=2, btn_y_id=3,
                       btn_left_id=4, btn_right_id=5,
                       btn_back_id=6, btn_start_id=7,
                       btn_power_id=8,
                       btn_left_stick_id=9, btn_right_stick_id=10,
                       axes_left_x_id=0, axes_left_y_id=1,
                       axes_left_trigger_id=2,
                       axes_right_x_id=3, axes_right_y_id=4,
                       axes_right_trigger_id=5,
                       axes_cross_x_id=6, axes_cross_y_id=7):

        '''
        Constructor

        :param btn_a_id: Int index of button a in JoyMsgs
        :param btn_b_id: Int index of button b in JoyMsgs
        :param btn_x_id: Int index of button x in JoyMsgs
        :param btn_y_id: Int index of button y in JoyMsgs
        :param btn_left_id:  Int index of button left in JoyMsgs
        :param btn_right_id: Int index of button right in JoyMsgs
        :param btn_back_id:  Int index of button back in JoyMsgs
        :param btn_start_id: Int index of button start in JoyMsgs
        :param btn_power_id: Int index of button power in JoyMsgs
        :param btn_left_stick_id:  Int index of left stick button in JoyMsgs
        :param btn_right_stick_id: Int index of right stick button in JoyMsgs
        :param axes_left_x_id:        Int index of axes left horizontal in JoyMsgs
        :param axes_left_y_id:        Int index of axes left vertical in JoyMsgs
        :param axes_left_trigger_id:  Int index of axes left trigger in JoyMsgs
        :param axes_right_x_id:       Int index of axes right horizontal in JoyMsgs
        :param axes_right_y_id:       Int index of axes right vertical in JoyMsgs
        :param axes_right_trigger_id: Int index of axes right trigger in JoyMsgs
        :param axes_cross_x_id:       Int index of axes cross horizontal in JoyMsgs
        :param axes_cross_y_id:       Int index of axes cross vertical in JoyMsgs
        '''

        # BUTTON AND AXES IDS
        self.btn_a_id = btn_a_id
        self.btn_b_id = btn_b_id
        self.btn_x_id = btn_x_id
        self.btn_y_id = btn_y_id
        self.btn_left_id = btn_left_id
        self.btn_right_id = btn_right_id

        self.btn_back_id = btn_back_id
        self.btn_start_id = btn_start_id
        self.btn_power_id = btn_power_id
        self.btn_left_stick_id = btn_left_stick_id
        self.btn_right_stick_id = btn_right_stick_id

        self.axes_left_x_id = axes_left_x_id
        self.axes_left_y_id = axes_left_y_id
        self.axes_left_trigger_id = axes_left_trigger_id
        self.axes_right_x_id = axes_right_x_id
        self.axes_right_y_id = axes_right_y_id
        self.axes_right_trigger_id = axes_right_trigger_id

        self.axes_cross_x_id = axes_cross_x_id
        self.axes_cross_y_id = axes_cross_y_id

        # BUTTON AND AXES VALUES
        self.btn_a_pressed = False
        self.btn_b_pressed = False
        self.btn_x_pressed = False
        self.btn_y_pressed = False
        self.btn_left_pressed = False
        self.btn_right_pressed = False

        self.btn_back_pressed = False
        self.btn_start_pressed = False
        self.btn_power_pressed = False
        self.btn_left_stick_pressed = False
        self.btn_right_stick_pressed = False

        self.axes_left_x = 0.0
        self.axes_left_y = 0.0
        self.axes_left_trigger = 1.0
        self.axes_right_x = 0.0
        self.axes_right_y = 0.0
        self.axes_right_trigger = 1.0

        self.axes_cross_x = 0.0
        self.axes_cross_y = 0.0

class NeuroracerTeleop(object):
    '''
    Class for racecar teleop rospy nodes
    '''

    def __init__(self, command_topic='/neuroracer_teleop_joy/cmd_vel', joy_topic='/joy',
                       drive_command_topic='/neuroracer_ai/output/drive_command', ai_state_topic='/neuroracer_ai/input/ai_state',
                       btn_a_id=0, btn_b_id=1,
                       btn_x_id=2, btn_y_id=3,
                       btn_left_id=4, btn_right_id=5,
                       btn_back_id=6, btn_start_id=7,
                       btn_power_id=8,
                       btn_left_stick_id=9, btn_right_stick_id=10,
                       axes_left_x_id=0, axes_left_y_id=1,
                       axes_left_trigger_id=2,
                       axes_right_x_id=3, axes_right_y_id=4,
                       axes_right_trigger_id=5,
                       axes_cross_x_id=6, axes_cross_y_id=7):

        '''
        Constructor

        :param command_topic:       String topic for the command publisher
        :param joy_topic:           String topic of the rosjoy node
        :param drive_command_topic: String topic of the ai drive commands
        :param ai_state_topic:      String topic for the ai state publisher
        :param btn_a_id: Int index of button a in JoyMsgs
        :param btn_b_id: Int index of button b in JoyMsgs
        :param btn_x_id: Int index of button x in JoyMsgs
        :param btn_y_id: Int index of button y in JoyMsgs
        :param btn_left_id:  Int index of button left in JoyMsgs
        :param btn_right_id: Int index of button right in JoyMsgs
        :param btn_back_id:  Int index of button back in JoyMsgs
        :param btn_start_id: Int index of button start in JoyMsgs
        :param btn_power_id: Int index of button power in JoyMsgs
        :param btn_left_stick_id:  Int index of left stick button in JoyMsgs
        :param btn_right_stick_id: Int index of right stick button in JoyMsgs
        :param axes_left_x_id:        Int index of axes left horizontal in JoyMsgs
        :param axes_left_y_id:        Int index of axes left vertical in JoyMsgs
        :param axes_left_trigger_id:  Int index of axes left trigger in JoyMsgs
        :param axes_right_x_id:       Int index of axes right horizontal in JoyMsgs
        :param axes_right_y_id:       Int index of axes right vertical in JoyMsgs
        :param axes_right_trigger_id: Int index of axes right trigger in JoyMsgs
        :param axes_cross_x_id:       Int index of axes cross horizontal in JoyMsgs
        :param axes_cross_y_id:       Int index of axes cross vertical in JoyMsgs
        '''

        self.joy = ControllerState(btn_a_id, btn_b_id,
                                   btn_x_id, btn_y_id,
                                   btn_left_id, btn_right_id,
                                   btn_back_id, btn_start_id,
                                   btn_power_id,
                                   btn_left_stick_id, btn_right_stick_id,
                                   axes_left_x_id, axes_left_y_id,
                                   axes_left_trigger_id,
                                   axes_right_x_id, axes_right_y_id,
                                   axes_right_trigger_id,
                                   axes_cross_x_id, axes_cross_y_id)

        self.zero_twist_published = False

        self.command_topic = command_topic
        self.joy_topic = joy_topic

        self.ai_state_flick_flack = False
        self.ai_state = False

        rospy.init_node('neuroracer_teleop_joy', anonymous=True)

        rospy.Subscriber(self.joy_topic, Joy, self.joyCallback)
        rospy.Subscriber(drive_command_topic, Twist, self.aiJoyCallback)

        self.publisherTeleop = rospy.Publisher(self.command_topic, Twist, queue_size=1, latch=True)
        rospy.Timer(rospy.Duration(0.1), self.publish)

        # do not use latch=True, if used AI changed its state even when the right button was not pressed
        self.publisherAiState =  rospy.Publisher(ai_state_topic, Bool, queue_size=1)
        rospy.Timer(rospy.Duration(0.1), self.aiStateCallbak)

        rospy.spin()

    def aiJoyCallback(self, data):
        '''
        Callback function for ai command msgs, published only if left button is not pressed

        :param data: TwistMsg predicted command msg
        '''

        # self.ai_state is need, or else the zero twist msg wouldn't work
        # it would publish the zero twist, but would also get some last
        # twist msg from Autonomous.py and so the engine would not stop
        if not self.joy.btn_left_pressed and self.ai_state:
            self.publisherTeleop.publish(data)
        elif not self.joy.btn_left_pressed and not self.ai_state:
            twist = Twist()
            twist.angular.z = 0.0
            twist.linear.x = 0.0
            self.publisherTeleop.publish(twist)

    def aiStateCallbak(self, event):
        '''
        Callback for ai state publisher, ai can be activated and deactivated with button right

        :param event: TimeEvent not used
        '''

        if self.joy.btn_right_pressed and not self.ai_state_flick_flack:
            self.ai_state = not self.ai_state
            self.publisherAiState.publish(self.ai_state)
            self.ai_state_flick_flack = True

        elif not self.joy.btn_right_pressed and self.ai_state_flick_flack:
            self.ai_state_flick_flack = False


    def joyCallback(self, data):
        '''
        Callback for rosjoy Subscriber updates self.joy

        :param data: JoyMsg published msg from rosjoy node

        Trigger values are initial 0.0 in sensor_msgs.msg Joy but should be 1.0
        remember this if you want to use the triggers, after first use of each trigger
        each value is correct
        '''
        self.joy.btn_a_pressed = data.buttons[self.joy.btn_a_id] == 1
        self.joy.btn_b_pressed = data.buttons[self.joy.btn_b_id] == 1
        self.joy.btn_x_pressed = data.buttons[self.joy.btn_x_id] == 1
        self.joy.btn_y_pressed = data.buttons[self.joy.btn_y_id] == 1
        self.joy.btn_left_pressed = data.buttons[self.joy.btn_left_id] == 1
        self.joy.btn_right_pressed = data.buttons[self.joy.btn_right_id] == 1

        self.joy.btn_back_pressed = data.buttons[self.joy.btn_back_id] == 1
        self.joy.btn_start_pressed = data.buttons[self.joy.btn_start_id] == 1
        self.joy.btn_power_pressed = data.buttons[self.joy.btn_power_id] == 1
        self.joy.btn_left_stick_pressed = data.buttons[self.joy.btn_left_stick_id] == 1
        self.joy.btn_right_stick_pressed = data.buttons[self.joy.btn_right_stick_id] == 1

        self.joy.axes_left_x = data.axes[self.joy.axes_left_x_id]
        self.joy.axes_left_y = data.axes[self.joy.axes_left_y_id]
        self.joy.axes_left_trigger = data.axes[self.joy.axes_left_trigger_id]
        self.joy.axes_right_x = data.axes[self.joy.axes_right_x_id]
        self.joy.axes_right_y = data.axes[self.joy.axes_right_y_id]
        self.joy.axes_right_trigger = data.axes[self.joy.axes_right_trigger_id]

        self.joy.axes_cross_x = data.axes[self.joy.axes_cross_x_id]
        self.joy.axes_cross_y = data.axes[self.joy.axes_cross_y_id]

        # self.log()


    def publish(self, event):
        '''
        Twist msg publishing function, published TwistMsgs if button left is pressed
        if button left is not pressed, one neutral TwistMsg is published

        :param event: TimeEvent not used
        '''

        if self.joy.btn_left_pressed:
            twist = Twist()
            twist.angular.z = SCALE_AXES_X * self.joy.axes_left_x
            twist.linear.x  = SCALE_AXES_Y * self.joy.axes_right_y
            self.publisherTeleop.publish(twist)
            self.zero_twist_published = False
        elif not self.joy.btn_left_pressed and not self.zero_twist_published:
            twist = Twist()
            twist.angular.z = 0.0
            twist.linear.x = 0.0
            self.publisherTeleop.publish(twist)
            self.zero_twist_published = True

    def log(self):
        '''
        Logs current ControllerState states
        '''

        rospy.logwarn('BUTTON A:      {}'.format(self.joy.btn_a_pressed))
        rospy.logwarn('BUTTON B:      {}'.format(self.joy.btn_b_pressed))
        rospy.logwarn('BUTTON X:      {}'.format(self.joy.btn_x_pressed))
        rospy.logwarn('BUTTON Y:      {}'.format(self.joy.btn_y_pressed))
        rospy.logwarn('BUTTON Left:   {}'.format(self.joy.btn_left_pressed))
        rospy.logwarn('BUTTON Right:  {}'.format(self.joy.btn_right_pressed))

        rospy.logwarn('BUTTON Back:   {}'.format(self.joy.btn_back_pressed))
        rospy.logwarn('BUTTON Start:  {}'.format(self.joy.btn_start_pressed))
        rospy.logwarn('BUTTON Power:  {}'.format(self.joy.btn_power_pressed))
        rospy.logwarn('BUTTON LS:     {}'.format(self.joy.btn_left_stick_pressed))
        rospy.logwarn('BUTTON RS:     {}'.format(self.joy.btn_right_stick_pressed))

        rospy.logwarn('AXES Left X:   {}'.format(self.joy.axes_left_x))
        rospy.logwarn('AXES Left Y:   {}'.format(self.joy.axes_left_y))
        rospy.logwarn('AXES Left T:   {}'.format(self.joy.axes_left_trigger))
        rospy.logwarn('AXES Right X:  {}'.format(self.joy.axes_right_x))
        rospy.logwarn('AXES Right Y:  {}'.format(self.joy.axes_right_y))
        rospy.logwarn('AXES Right T:  {}'.format(self.joy.axes_right_trigger))

        rospy.logwarn('AXES Cross X:  {}'.format(self.joy.axes_cross_x))
        rospy.logwarn('AXES Cross Y:  {}'.format(self.joy.axes_cross_y))

if __name__ == '__main__':
    '''
    Main
    loads yaml config files and sets the parameter for NeuroracerTeleop node

    :raise IOError if one config file can not be loaded
    '''

    root_path = os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.abspath(
                                __file__))))

    teleop_config_file_path = root_path + '/configs/teleop/teleop.yaml'
    joy_config_file_path = root_path + '/configs/joy/joy.yaml'

    teleop_cfg = load_yaml(teleop_config_file_path)
    joy_cfg = load_yaml(joy_config_file_path)

    if joy_cfg is not None and teleop_cfg is not None:
        teleop = NeuroracerTeleop(command_topic=teleop_cfg['topic'], joy_topic=joy_cfg['topic'],
                                  drive_command_topic=teleop_cfg['drive_command_topic'], ai_state_topic=teleop_cfg['ai_state_topic'],
                                  btn_a_id=teleop_cfg['btn_a_id'], btn_b_id=teleop_cfg['btn_b_id'],
                                  btn_x_id=teleop_cfg['btn_x_id'], btn_y_id=teleop_cfg['btn_y_id'],
                                  btn_left_id=teleop_cfg['btn_left_id'], btn_right_id=teleop_cfg['btn_right_id'],
                                  btn_back_id=teleop_cfg['btn_back_id'], btn_start_id=teleop_cfg['btn_start_id'],
                                  btn_power_id=teleop_cfg['btn_power_id'],
                                  btn_left_stick_id=teleop_cfg['btn_left_stick_id'], btn_right_stick_id=teleop_cfg['btn_right_stick_id'],
                                  axes_left_x_id=teleop_cfg['axes_left_x_id'], axes_left_y_id=teleop_cfg['axes_left_y_id'],
                                  axes_left_trigger_id=teleop_cfg['axes_left_trigger_id'],
                                  axes_right_x_id=teleop_cfg['axes_right_x_id'], axes_right_y_id=teleop_cfg['axes_right_y_id'],
                                  axes_right_trigger_id=teleop_cfg['axes_right_trigger_id'],
                                  axes_cross_x_id=teleop_cfg['axes_cross_x_id'], axes_cross_y_id=teleop_cfg['axes_cross_y_id'])
    else:
        raise IOError('Config file(s) not found: {} or {}'.format( teleop_config_file_path, joy_config_file_path ))
