/*
  Arduino ROS node for the NeuroRacer project
  The Arduino controls a HIMOTO Katana Car
  MIT License
  HTW Berlin (2016-2017)
*/

#include <Servo.h>

#define USE_USBCON
#include <ros.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Empty.h>
#include <geometry_msgs/Twist.h>

#define PIN_SERVO 9
#define PIN_ESC 10
#define PIN_LED 5

#define LET_LED(set) digitalWrite(PIN_LED, (set == 0 ? LOW : (set == 1 ? HIGH : HIGH-digitalRead(PIN_LED))));

std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);

ros::NodeHandle nodeHandle;

Servo steeringServo;
Servo motorESC; // The Electronic Speed Controller (ESC) works like a Servo

// General bounds for the steering servo and the ESC
const int servoNeutral = 90;
const int diffSteer = 20;
const int diffThrottle = 15; // < this shouldn't be lower than 12, if lower backwards doesn't works so good
const int minSteering = servoNeutral - diffSteer;
const int maxSteering = servoNeutral + diffSteer;
const int minThrottle = servoNeutral - diffThrottle;
const int maxThrottle = servoNeutral + diffThrottle;

int initialized = 0;

int msgCnt = 0;
char buffer[256];
void chatterMessage(const char* str) {
  str_msg.data = str;
  chatter.publish(&str_msg);
}

void blink(int _delay, int count = 1) {
  for (int i = 0; i < count; i++) {
    digitalWrite(PIN_LED, HIGH);
    delay(_delay);
    digitalWrite(PIN_LED, LOW);
    delay(_delay);
  }
}

/**
 * Arduino 'map' funtion for floating point
 */
double fmap (double x, double in_min, double in_max, double out_min, double out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/**
 * Combined MIN and MAX.
 */
inline int between(int x, int minimum, int maximum) {
  return x < minimum ? minimum : (x > maximum ? maximum : x);
}

/**
 * Map steering input to a valid servo angle.
 *
 * @param double steer Value from -1.0 to 1.0
 * @return int Steering angle
 */
int mapSteering(double steer) {
  int servoAngle = fmap(steer, -1.0, 1.0, minSteering, maxSteering);
  return between(servoAngle, minSteering, maxSteering);
}

/**
 * Map throttle input to a valid servo angle for the ESC.
 *
 * @param double throttle Value from -1.0 to 1.0
 * @return int Throttle angle / value
 */
int mapThrottle(double throttle) {
  int servoThrottle = fmap(throttle, -1.0, 1.0, minThrottle, maxThrottle);  
  return between(servoThrottle, minThrottle, maxThrottle);
}

void driveCallback(const geometry_msgs::Twist&  twistMsg) {
  const double steerIn = twistMsg.angular.z; // + 0.5; // +0.5 changed for neuroracer
  const double throttleIn = twistMsg.linear.x; // + 0.5; // +0.5 changed for neuroracer
  const int steer = mapSteering(steerIn);
  const int throttle = mapThrottle(throttleIn);
  steeringServo.write(steer);
  motorESC.write(throttle);
  
  msgCnt = (msgCnt+1) % 100;
  sprintf(buffer, "[%d] steer: %d/100 -> %d, throttle: %d/100 -> %d \n", msgCnt, (int)(steerIn*100), steer, (int)(throttleIn*100), throttle);
  chatterMessage(buffer);
  
//  delay(100);
  //if (initialized) {
    //steeringServo.write(mapSteering(twistMsg.angular.z));
    //motorESC.write(mapThrottle(twistMsg.linear.x));
    //LET_LED(2);
  //} else {
//    blink(200, 1);
  //}
}

ros::Subscriber<geometry_msgs::Twist> driveSubscriber("/neuroracer_teleop_joy/cmd_vel", &driveCallback);

void setup() {
  pinMode(PIN_LED, OUTPUT);
  Serial.begin(115200); // 115200 // 57600
  
  nodeHandle.initNode();
  nodeHandle.advertise(chatter); // debug chatter
  nodeHandle.subscribe(driveSubscriber) ;    
  
  // attach the steering servo and the ESC, send neutral values
  steeringServo.attach(PIN_SERVO);
  motorESC.attach(PIN_ESC);
  steeringServo.write(servoNeutral);
  motorESC.write(servoNeutral);
  blink(500, 10);
}

void testSteering() {
  double steer[] = { 0.5, 0.75, 1.0, 0.75, 0.5, 0.25, 0.0, 0.25, 0.5 };
  for (int i = 0; i < 9; i++) {
    delay(1000);
    LET_LED(2);
    for (int j = 0; j < 100; j++) {
      steeringServo.write(mapSteering(steer[i]));
      delay(1);
    }
  }
}

void loop() {
  nodeHandle.spinOnce();
  delay(1);
  //testSteering();
}

