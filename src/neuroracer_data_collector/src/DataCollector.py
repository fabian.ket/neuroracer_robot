#!/usr/bin/env python

import sys
import time
from collections import deque

import rospy
import rosbag
from sensor_msgs.msg import Joy, Image, CompressedImage
from ackermann_msgs.msg import AckermannDriveStamped
from geometry_msgs.msg import Twist

import roslaunch

import os
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.dirname(
                                os.path.dirname(os.path.abspath(__file__))
                            )
                        )
                    )
                ))

from neuroracer_utils.YamlLoader import load_yaml

class DataCollector:
    '''
    Class for collecting camera and control msgs, saves them in a rosbag file
    '''

    def __init__(self, cam_topic, command_topic, joy_topic, path, img_type='compr', simulation=True, btn_start_id=7, btn_back_id=6):
        '''
        Constructor

        :param cam_topic:     String topic of the camera publisher
        :param command_topic: String topic of the teleop node
        :param joy_topic:     String topic of the rosjoy node
        :param path:          String save path for rosbag file
        :param img_type:      String type of published images (compr/raw)
        :param simulation:    Bool to decide which type command msgs are published
        :param btn_start_id:  Int index of the start button in JoyMsgs
        :param btn_back_id:   Int index of the back button in JoyMsgs

        :raise IOError if path is not an dir
        '''

        if not os.path.isdir(path):
            raise IOError('Not such dir: {}'.format( path ))

        self.event_queue = deque() # dequeue for image and command msgs
        self.bag = None

        self.cam_topic = cam_topic
        self.command_topic = command_topic
        self.joy_topic = joy_topic

        self.path = os.path.abspath(path) + '/'

        self.img_type = img_type

        self.simulation = simulation

        self.btn_start_id = btn_start_id
        self.btn_back_id = btn_back_id

        self.btn_start_prev_state = 0
        self.btn_back_prev_state = 0

        self.terminate_writing = 0
        self.restart_writing = 0

        self.write_bag = False

        cam_msg_type = Image if self.img_type == 'raw' else CompressedImage
        rospy.Subscriber(self.cam_topic,
                         cam_msg_type,
                         self.image_callback)

        command_msg_type = AckermannDriveStamped if self.simulation else Twist
        rospy.Subscriber(self.command_topic,
                         command_msg_type,
                         self.command_callback)

        rospy.Subscriber(self.joy_topic,
                         Joy,
                         self.joy_callback)

    # def camera_launcher(self):
    #     uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    #     roslaunch.configure_logging(uuid)
    #
    #     launch = roslaunch.parent.ROSLaunchParent(uuid, self.camera_launchers)
    #     launch.start()

    def command_callback(self, msg):
        '''
        Callback for command msgs, appends them to the dequeue

        :param msg: AckermannMsg/TwistMsg received command msg
        '''
        if self.write_bag:
            # rospy.logwarn('++++++++++++++++++++++++++++ writing command to queue')
            self.event_queue.append(msg)

    def image_callback(self, msg):
        '''
        Callback for image msgs, appends them to the dequeue

        :param msg: Image/CompressedImage received image msg
        '''

        if self.write_bag:
            # rospy.logwarn('============================ writing image to queue')
            self.event_queue.append(msg)

    def joy_callback(self, msg):
        '''
        Callback for joy msgs, starts, stops or restarts data collecting

        :param msg: JoyMsg from the rosjoy node
        '''

        if msg.buttons[self.btn_start_id] == 1 and self.btn_start_prev_state == 0:  # start button
            self.btn_start_prev_state = 1
            self.restart_writing = 1
            rospy.logwarn('====== START DATA COLLECTING')
        else:
            self.btn_start_prev_state = msg.buttons[self.btn_start_id]

        if msg.buttons[self.btn_back_id] == 1 and self.btn_back_prev_state == 0:  # back button
            self.btn_back_prev_state = 1
            self.terminate_writing = 1
            rospy.logwarn('====== STOP DATA COLLECTING')
        else:
            self.btn_back_prev_state = msg.buttons[self.btn_back_id]

    def save_log(self):
        '''
        Pops a msg from the dequeue and write topic and msg in the rosbag file
        rosbag filename is a timestamp
        '''

        try:
            if self.write_bag:
                message = self.event_queue.pop()

                if isinstance(message, Image):
                    self.bag.write(self.cam_topic, message)

                elif isinstance(message, CompressedImage):
                    self.bag.write(self.cam_topic, message)

                elif isinstance(message, AckermannDriveStamped):
                    self.bag.write(self.command_topic, message)

                elif isinstance(message, Twist):
                    self.bag.write(self.command_topic, message)

                else:
                    raise Exception("Unknown message type: " + type(message))

            if self.terminate_writing:
                if self.bag is not None:
                    self.write_bag = False
                    self.bag.close()
                    self.bag = None
                    self.event_queue.clear()
                    self.terminate_writing = 0

            if self.restart_writing:
                if self.bag is not None:
                    self.write_bag = False
                    self.bag.close()
                    self.bag = None

                self.bag = rosbag.Bag(self.path + 'bag' + time.strftime("%d-%m-%Y-%H:%M:%S") + '.bag', 'w')
                self.event_queue.clear()
                self.write_bag = True
                self.restart_writing = 0

        except IndexError:
            pass


def main():
    '''
    Main function
    loads yaml config files and sets the DataCollector parameter

    :raise IOError if one of the config files can not be loaded
    '''

    root_path = os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.abspath(
                                __file__))))

    data_collector_config_file_path = root_path + '/configs/data_collector/data_collector.yaml'
    camera_config_file_path = root_path + '/configs/camera/camera.yaml'
    joy_config_file_path = root_path + '/configs/joy/joy.yaml'
    teleop_config_file_path = root_path + '/configs/teleop/teleop.yaml'

    data_collector_cfg = load_yaml(data_collector_config_file_path)
    camera_cfg = load_yaml(camera_config_file_path)
    joy_cfg = load_yaml(joy_config_file_path)
    teleop_cfg = load_yaml(teleop_config_file_path)

    if data_collector_cfg is not None and camera_cfg is not None and joy_cfg is not None and teleop_cfg is not None:

        data_collector = None

        try:
            rospy.init_node('data_collector')

            data_collector = DataCollector(camera_cfg['topic'],
                                           teleop_cfg['topic'],
                                           joy_cfg['topic'],
                                           data_collector_cfg['path'],
                                           camera_cfg['type'],
                                           data_collector_cfg['simulation'],
                                           teleop_cfg['btn_start_id'],
                                           teleop_cfg['btn_back_id'])

            # Spin until ctrl + c
            while not rospy.is_shutdown():
                data_collector.save_log()
        finally:
            if data_collector is not None and data_collector.bag is not None:
                data_collector.write_bag = False
                data_collector.bag.close()
                data_collector.bag = None

    elif data_collector_cfg is None:
        raise IOError('Config file not found: {}'.format( data_collector_config_file_path ))
    elif camera_cfg is None:
        raise IOError('Config file not found: {}'.format( camera_config_file_path ))
    elif joy_cfg is None:
        raise IOError('Config file not found: {}'.format( joy_config_file_path ))
    elif teleop_cfg is None:
        raise IOError('Config file not found: {}'.format( teleop_config_file_path ))


if __name__ == '__main__':
    main()
