#!/usr/bin/env python
import cv2
import rospy
from sensor_msgs.msg import Image, CompressedImage
import time

import os
import sys
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.dirname(
                                os.path.dirname(os.path.abspath(__file__))
                            )
                        )
                    )
                ))

from neuroracer_utils.YamlLoader import load_yaml
from neuroracer_utils.CvBridgeHandler import *

class CameraStreamer(object):
    '''
    CameraStreamer class to stream the camera images with fps display
    '''

    def __init__(self, topic='/camera/front/image_rect_color/compressed', img_type='compr',
                 queue_size=1, sec_to_avg_over=1):

        '''
        Constructor

        :param topic:      String camera publisher topic
        :param img_type:   String type of published images (compr/raw)
        :param queue_size: Int queue_size of camera subscriber
        :param sec_to_avg_over: Int update periode for fps display
        '''

        self.topic = topic
        self.img_type = img_type

        self.queue_size = queue_size

        self.window_name = 'CameraStreamer'

        self.cv_bridge_handler = CvBridgeHandlerCompr() if self.img_type == 'compr' else CvBridgeHandlerRaw()

        self.time = time.time()
        self.sec_to_avg_over = sec_to_avg_over
        self.img_count = 0
        self.fps = 0.0

    def callback(self, msg):
        '''
        Callback function for camera subscriber, displays camera image and fps

        :param msg: image msg from camera publisher
        '''

        image = self.cv_bridge_handler.get_image(msg)
        self.img_count += 1

        cv2.putText(image,
                    'FPS: {}'.format( self.fps ),
                    (15,25),
                    cv2.FONT_HERSHEY_PLAIN,
                    1.0, (255,255,0), 2 )

        cv2.imshow(self.window_name, image)
        cv2.waitKey(1)

        delta_time = time.time() - self.time
        if delta_time >= self.sec_to_avg_over:
            self.fps = round(self.img_count / delta_time, 2 )
            self.img_count = 0
            self.time = time.time()

    def listen(self):
        '''
        Init function for camera subscriber
        '''

        rospy.init_node('neuroracer_camera_streamer', anonymous=True)

        sub_msg = Image if self.img_type == 'raw' else CompressedImage
        rospy.Subscriber(self.topic,
                         sub_msg,
                         self.callback,
                         queue_size=self.queue_size)

        rospy.spin()

if __name__ == '__main__':
    '''
    Main
    loads yaml config files and sets streamer parameter

    :raise IOError if yaml config can not be loaded
    '''

    root_path = os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.abspath(
                                __file__))))

    pub_config_file_path = root_path + '/configs/camera/camera.yaml'
    sub_config_file_path = root_path + '/configs/camera/streamer.yaml'

    cam_pub_cfg = load_yaml(pub_config_file_path)
    cam_sub_cfg = load_yaml(sub_config_file_path)

    if cam_pub_cfg is not None and cam_sub_cfg is not None:
        cam_streamer = CameraStreamer(topic=cam_pub_cfg['topic'], img_type=cam_pub_cfg['type'],
                                      queue_size=cam_sub_cfg['queue_size'], sec_to_avg_over=cam_sub_cfg['sec_to_avg_over'])
        cam_streamer.listen()
        cv2.destroyAllWindows()
    else:
        raise IOError('Config file(s) not found: {} or {}'.format( pub_config_file_path, sub_config_file_path ))
