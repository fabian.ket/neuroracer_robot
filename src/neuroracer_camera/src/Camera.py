#!/usr/bin/env python

import cv2
import rospy
from sensor_msgs.msg import Image, CompressedImage

import os
import sys
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.dirname(
                                os.path.dirname(os.path.abspath(__file__))
                            )
                        )
                    )
                ))

from neuroracer_utils.YamlLoader import load_yaml
from neuroracer_utils.CvBridgeHandler import *

# neuroracer-robot
# PIPE = "nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720,format=(string)I420, framerate=(fraction)120/1 ! nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"
# PIPE = "nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720,format=(string)I420, framerate=(fraction)30/1 ! nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"

# USB
# PIPE = 0

class Camera:
    '''
    Class for rospy camera nodes
    '''

    def __init__(self, topic='/camera/front/image_rect_color/compressed', img_type='compr',
                 img_width=1280, img_height=720,
                 queue_size=1):

        '''
        Constructor

        :param topic:      String topic for the camera publisher
        :param img_type:   String type of the published images (compr/raw)
        :param img_width:  Int width of the published images
        :param img_height: Int height of the published images
        :param queue_size: Int queue_size of the publisher

        :raise Exception if camera stream can not be opened
        '''

        self.topic = topic
        self.img_type = img_type

        self.img_width = img_width
        self.img_height = img_height

        self.queue_size = queue_size

        self.cv_bridge_handler = CvBridgeHandlerCompr() if self.img_type == 'compr' else CvBridgeHandlerRaw()

        # nvidia camera src pipe
        self.cam_pipe = ('nvcamerasrc ! video/x-raw(memory:NVMM), ' +
                         'width=(int){}, height=(int){},format=(string)I420, framerate=(fraction)120/1 ! '.format( self.img_width, self.img_height ) +
                         'nvvidconv flip-method=0 ! video/x-raw, format=(string)BGRx ! ' +
                         'videoconvert ! video/x-raw, format=(string)BGR ! appsink')

        # camera stream
        self.input_stream = cv2.VideoCapture(self.cam_pipe)
        if not self.input_stream.isOpened():
            raise Exception('Camera stream did not open\n')


        pub_msg = Image if self.img_type == 'raw' else CompressedImage
        self.publisher = rospy.Publisher(self.topic,
                                         pub_msg,
                                         queue_size=self.queue_size)

    def publish(self, verbose=0):
        '''
        Published images from the camera stream

        :param verbose: Int if set logs msg infos
        '''

        while not rospy.is_shutdown() and self.input_stream.isOpened():
            success, frame = self.input_stream.read()
            msg_frame = self.cv_bridge_handler.get_msg(frame)
            self.publisher.publish(msg_frame)
            if verbose:
                self.cv_bridge_handler.log(msg_frame)

    def calibrate_camera(self, verbose=0):
        pass


def main():
    '''
    Main function
    loads config files and starts camera node

    :raise IOError if config file can not be loaded
    '''

    root_path = os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.abspath(
                                __file__))))

    pub_config_file_path = root_path + '/configs/camera/camera.yaml'

    pub_cfg = load_yaml(pub_config_file_path)

    if pub_cfg is not None:
        # default values
        calibrate = pub_cfg['calibrate']
        verbose = pub_cfg['verbose'] # use 1 for debug

        try:
            # register node
            rospy.init_node('neuroracer_camera', anonymous=True)

            cam = Camera(topic=pub_cfg['topic'], img_type=pub_cfg['type'],
                         img_width=pub_cfg['img_width'], img_height=pub_cfg['img_height'],
                         queue_size=pub_cfg['queue_size'])

            if calibrate:
                cam.calibrate_camera(verbose)

            cam.publish(verbose)

        except rospy.ROSInterruptException:
            pass
    else:
        raise IOError('Config file not found: {}'.format(pub_config_file_path))


if __name__ == '__main__':
    main()
