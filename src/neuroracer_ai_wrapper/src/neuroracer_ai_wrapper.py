#!/usr/bin/env python

import os
import sys

sys.path.append (os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.dirname(
                                os.path.dirname(os.path.abspath(__file__))
                            )
                        )
                    )
                ))

from neuroracer_ai.src.playfieldAI import main as ai

def main():
    """
    Wrapper to start AI
    """
    ai()


if __name__ == '__main__':
    main()
