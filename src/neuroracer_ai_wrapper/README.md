### wrapper to start neuroracer-ai

1. create symlink in 'src' folder from 'neuroracer-ai/src' (Note: it is necessary to name the created symlink 'neuroracer_ai'):
ln -s absolute\_path/to/neuroracer-ai/src absolute\_path/to/neuroracer-robot/src/neuroracer\_ai\_wrapper/src/neuroracer_ai
2. activate neuroracer-ai-wrapper launch inside neuroracer.launch
